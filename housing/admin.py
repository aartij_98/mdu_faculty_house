from django.contrib import admin
from .models import Room, Status#, Booking
from django.db.models import Q
from datetime import datetime,date
# Register your models here.

# class StatusInline(admin.TabularInline):
#     model = Status
#
# class RoomAdmin(admin.ModelAdmin):
#     inlines = [StatusInline]
#
# class RoomInline(admin.TabularInline):
#     model = Room
#
# class Statusadmin(admin.ModelAdmin):
#     inlines = [RoomInline]

# class FilterBookings(admin.)

class PostStatus(admin.ModelAdmin):
    list_filter = ['status','checkin','checkout','room','booked_for']

    def get_queryset(self,request):
        qs = super(PostStatus,self).get_queryset(request)
        today = date.today()
        if today.month == 1:
            today = today.replace(month=11)
        elif today.month == 2:
            today = today.replace(month=12)
        else:
            today = today.replace(month=today.month-2)
        # today.replace(month=self.month - 2)
        return qs.filter(checkin__gte = today)


admin.site.register(Room)
admin.site.register(Status,PostStatus)
# admin.site.register(Status, Statusadmin)
