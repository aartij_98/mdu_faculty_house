#!/bin/bash
NAME=FacultyHousing
DJANGODIR=/home/igu/Projects/MduFaculty             # Django project directory
SOCKFILE=/home/igu/Projects/MduFaculty/run/gunicorn.sock  # we will communicte using this unix socket
USER=igu                                        # the user to run as
GROUP=igu                                     # the group to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=FacultyHouse.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=FacultyHouse.wsgi                     # WSGI module name
 
echo "Starting $NAME as `whoami`"

# Activate the virtual environment
##################################
#source /home/ubuntu/.local/bin/virtualenvwrapper.sh
export WORKON_HOME="$HOME/.virtualenvs"
export PROJECT_HOME="$HOME/Projects"

source /usr/share/virtualenvwrapper/virtualenvwrapper.sh

workon housing
#################################



# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --bind=unix:$SOCKFILE \
  --log-level=debug \
  --log-file=- & 

